# this
CC = g++
CFLAGS = -std=c++11 -Wall -Wextra -O3

default: build

game.o: game.cc game.hh
	$(CC) $(CFLAGS) -c game.cc
	
build: game.o
	$(CC) $(CFLAGS) -o game game.o

clean:
	rm *.o game