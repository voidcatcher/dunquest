# dunquest

welcome to dunquest, the small roguelike written in c++ made for your
terminal.

### what to do

- build the repository:
```
git clone http://gitlab.com/virtualsuckslol/dunquest
cd dunquest
make
./game
```

### what is it about

you walk into a dungeon knowing nothing about it. why must we tell you more?
explore...

### controls
```
w: move up
a: move left
s: move down
d: move right
c: check stats
enter: complete command
f: fight enemy (not recommended just walk to the enemy and it'll fight automatically)
```
