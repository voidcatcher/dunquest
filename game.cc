#include <iostream>
#include <random>
#include <string>
#include <algorithm>

// stuff bc calling twice isnt cool
#include <cstdlib>
#include <ctime>

#include "game.hh"

room gen_room(){
    
    static std::random_device rando;
    static std::mt19937 gen(rando());

    std::uniform_int_distribution<int> stuffgen(13, 23);
    int x = stuffgen(gen); int y = stuffgen(gen);
    int sx = x - 5; int sy = y - 5;
    int ix = sx - 3; int iy = sy - 3;
    int ex = ix + 2; int ey = iy + 2;
    int px = ex + 2; int py = ey + 2;
    
    std::shuffle(std::begin(items), std::end(items), gen);
    
    std::shuffle(std::begin(enemies), std::end(enemies), gen);
    
    std::shuffle(std::begin(potions), std::end(potions), gen);
    
    if(potions[0].title == "Mystery Potion"){
        std::uniform_int_distribution<int> potiongen(-10, 10);
        
        potions[0].power = potiongen(gen); 
    }
    
    if(potions[0].title == "None"){
        px = 0;
        py = 0;
    }
    
    enemies[0].x = ex;
    enemies[0].y = ey;
    
    room r = {x, y, sx, sy, items[0], ix, iy, enemies[0], potions[0], px, py};
    
    if(r.i.title == "None"){
        r.ix = 0;
        r.iy = 0;
    } else {
        r.ix = r.ix;
        r.iy = r.iy;
    }
    
    return r;

}

int main(){
    std::string input; // commands
    std::string sinput; // y/n thing for stairs
    std::string iinput; // y/n thing for items
    std::string finput; // input for fighting
    std::string pinput; // input for potions
    srand(time(NULL));
    room r = gen_room(); // room
    
    goto loop;
        
collision:
    if(pl.x == r.x){ pl.x--; goto loop; }
    if(pl.y == r.y){ pl.y--; goto loop; }
    if(pl.x < 2){ pl.x++; goto loop; }
    if(pl.y < 2){ pl.y++; goto loop; }

stair_check:
    if((pl.x == r.sx) && (pl.y == r.sy)){
        std::cout << "Walk into room? [y/n]: ";
        std::cin >> sinput;
        if(sinput == "y"){ r = gen_room(); goto loop; }
        if(sinput == "n"){ r = r; goto loop; }
    }

item_check:
    if((pl.x == r.ix) && (pl.y == r.iy)){
        std::cout << "Equip item? [y/n]: ";      
        std::cin >> iinput;
        if(iinput == "y"){
            pl.current.title = r.i.title;
            pl.power = r.i.power;
            r.i.title = "None";
            r.i.power = 0;
            r.i.weight = 0;
            goto loop;
        }
        
        if(iinput == "n"){ goto loop; }
    }

potion_check:
    if((pl.x == r.px) && (pl.y == r.py)){
        std::cout << "Equip potion? [y/n]: ";      
        std::cin >> pinput;
        if(pinput == "y"){
            pl.life += r.p.power;
            r.p.title = "None";
            r.p.power = 0;
            goto loop;
        }
        
        if(iinput == "n"){ goto loop; }
    }

enemy_move:
    if(!(r.e.title == "None")){   
        int choice = rand() % 1; // one and zero
        int m = rand() % 3;
        
        switch(choice){
            case 0:
                r.e.x += m;
                r.e.y -= m;
                break;
            case 1:
                r.e.x -= m;
                r.e.y += m;
                break;
        }
        
        if((r.e.x >= r.x)||(r.e.y >= r.y)||((r.e.x >= r.x)&&(r.e.y >= r.y))){
            r.e.x--;
            r.e.y--;
        }
        goto loop;
    } else {
        r.e.x = 0;
        r.e.y = 0;
        goto loop;
    }


enemy_fight:
    if((pl.x == r.e.x) && (pl.y == r.e.y)){
        r.e.life -= pl.power;
        pl.life -= r.e.power;
        
        std::cout << "Enemy Life: " << r.e.life << " | Enemy Power: " << r.e.power << std::endl;
        if(r.e.life <= 0){
            std::cout << "You killed the enemy." << std::endl;
            r.e.life = 0;
            r.e.power = 0;
            goto loop;
        }
        goto loop;
    } else {
        std::cout << "There isn't anyone to fight!" << std::endl;
        goto loop;
    }
        
    
loop:
    do {
        std::cin >> input;
        
        if(pl.life <= 0){
            std::cout << "Game Over!" << std::endl;
            exit(1);
        }
        
        if(input == "w"){ pl.y++; std::cout << "x: " << pl.x << " | " << "y: " << pl.y << std::endl; goto collision; goto item_check; goto potion_check; goto stair_check; goto enemy_move; goto loop; }
        if(input == "a"){ pl.x--; std::cout << "x: " << pl.x << " | " << "y: " << pl.y << std::endl; goto collision; goto item_check; goto potion_check; goto stair_check; goto enemy_move; goto loop; }
        if(input == "s"){ pl.y--; std::cout << "x: " << pl.x << " | " << "y: " << pl.y << std::endl; goto collision; goto item_check; goto potion_check; goto stair_check; goto enemy_move; goto loop; }
        if(input == "d"){ pl.x++; std::cout << "x: " << pl.x << " | " << "y: " << pl.y << std::endl; goto collision; goto item_check; goto potion_check; goto stair_check; goto enemy_move; goto loop; }
        if(input == "f"){ goto enemy_fight; }
        
        if(input == "c"){
            // standard stuff
            std::cout << "Life: " << pl.life << " | Strength: " << pl.power << " | Weight: " << pl.maxweight << std::endl;
            std::cout << "Player X: " << pl.x << " | Player Y: " << pl.y << std::endl;
            std::cout << "Player Item: " << pl.current.title << std::endl;
            // room stuff
            std::cout << "Room X: " << r.x << " | Room Y: " << r.y << std::endl;
            std::cout << "Stair X: " << r.sx << " | Stair Y: " << r.sy << std::endl;
            // item stuff
            std::cout << "Item: " << r.i.title << " | Weight: " << r.i.weight << " | Power: " << r.i.power << std::endl;
            std::cout << "Item X: " << r.ix << " | Item Y: " << r.iy << std::endl;
            // enemy stuff
            std::cout << "Enemy Name: " << r.e.title << std::endl;
            std::cout << "Enemy X: " << r.e.x << " | Enemy Y: " << r.e.y << std::endl;
            // potion stuff
            std::cout << "Potion Name: " << r.p.title << " | Potion Power: " << r.p.power << std::endl;
            std::cout << "Potion X: " << r.px << " | Potion Y: " << r.py << std::endl;
            goto loop; 
        }    
        
    } while(1);
    
    
    return 0;
}