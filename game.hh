#ifndef GAME_HH
#define GAME_HH

#include <string>
#include <vector>

struct item {

    std::string title;
    int weight;
    int power; 

} static items[] = { {"None", 0, 0}, {"Sword", 5, 5}, {"Longsword", 7, 7}, {"Dagger", 3, 3} };

struct potion {
    
    std::string title;
    int power;

} static potions[] = { {"None", 0}, {"Small Health Potion", 5}, {"Big Health Potion", 10}, {"Mystery Potion", 0} };

struct enemy {

    std::string title;
    int life;
    int power;
    int x;
    int y;
        
} static enemies[] = { {"None", 0, 0, 0, 0}, {"Orc", 10, 5, 0, 0}, {"Troll", 15, 5, 0, 0}, {"Gnome", 10, 7, 0, 0} };

struct room {
    
    int x; // size
    int y;
    int sx; // location of stairs
    int sy;
    item i;
    int ix; // item location
    int iy;
    enemy e;
    potion p;
    int px; // potion location
    int py;    
};

struct player {

    int life; // life points
    int power; // strength
    int maxweight; // maximum weight that can be carried
    int x; // location
    int y;
    item current;
    potion p;
    
} static pl = {30, 0, 20, 2, 2, {}, {}};

room gen_room();

#endif